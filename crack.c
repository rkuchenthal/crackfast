#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "md5.h"


const int PASS_LEN=50;   // Maximum any password can be

const int HASH_LEN=33;        // Length of MD5 hash strings


////////////////////////////////////////////////////////////////////////////////////////////
// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char password[PASS_LEN];
    char hash[HASH_LEN];
};
//////////////////////////////////////////////////////////////////////////////////////////////
int crackFunction(const void *a, const void *b)
{
    
    return strcmp(a, ((struct entry *)b)->hash);
    
}
//////////////////////////////////////////////////////////////////////////////////////////
int hashcmp(const void *target, const void *elem)
{
    
    return strcmp((*(struct entry *)target).hash, (*(struct entry *)elem).hash);
    
}

//////////////////////////////////////////////////////////////////////////////////////

int  fileLen(char *filename)
{

    struct stat info;
    
    int rett = stat( filename, &info );
    
    if(rett == -1)
        {
            
            return -1;
            
         }
    
    else
        {
            
            return info.st_size;
            
         }

}

//////////////////////////////////////////////////////////////////////////////////

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.

struct entry *read_dict( char *filename, int *size )
{
    
    int length = fileLen( filename );
    
    char *k = malloc( length );
    
    
    FILE *g = fopen( filename,"r" );
    
    if(!g)
        {
            
            printf( " Can't open %s for reading.\n", filename );
            
            exit(1);
            
         }


    fread(k, sizeof(char), length, g);
    
    fclose(g);
    
    
    int num = 0;
    
    
    for( int i = 0; i < length; i++ )
        {
            
            if( k[i] == '\n' )
                {
                    
                    k[i] = '\0';
                    
                    num++;
                    
                 }
            
         }
    
    
    // Allocate space for array of entry structs
    
     struct entry *ps = malloc( num * sizeof(struct entry ));
     
     // Load array with entry info
     
    char *football = &k[0];
    
    char *dj = md5( football, strlen( football ));
    
    strcpy( ps[0].password, football );
    
    strcpy( ps[0].hash, dj );
    
     
     
    int j = 1;
    
    for( int i = 0; i < length-1; i++ )
        {
            
            if ( k[i] == '\0' )
                {
                    
                    char *something = &k[i+1];
                    
                    char *hashed = md5( something, strlen( something ) );
                    
                    strcpy( ps[j].password, something );
                    
                    strcpy( ps[j].hash, hashed ) ;
                    
                    j++;
                    
                 }
             
        }
    
    *size = num;
    
    return ps;
    
    
 }

////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    
    if ( argc < 3 )
        {
            
            printf("Usage: %s hash_file dict_file\n", argv[0]);
            
            exit(1);
            
         }

    // TODO: Read the dictionary file into an array of entry structures
    
    int dlen = 0;
    
    struct entry *dictionary = read_dict(argv[2], &dlen);
    
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    
    qsort(dictionary, dlen, sizeof(struct entry), hashcmp);

    // TODO
    // Open the hash file for reading.
    
    FILE *f = fopen(argv[1],"r");
    
    if (!f)
        {
            
            printf("Can't open %s for reading.\n", argv[1]);
            
            exit(1);
            
         }
    
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    
    //struct entry *searcher = bsearch(hash_line, dictionary, dlen, sizeof(struct entry), crackFunction);
    
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    
    int counter = 0;
    
    char hash_line[100];
    
    while(fgets(hash_line, 100, f) != NULL)
        {
            
            if (hash_line[strlen(hash_line)-1] == '\n')
                {
                    
                    hash_line[strlen(hash_line)-1] = '\0';
                    
                 }
            
             struct entry *searcher = bsearch( hash_line, dictionary, dlen, sizeof(struct entry), crackFunction );
            
           if( searcher != NULL )
                {
                    
                    printf( "%s %s\n", searcher->password, hash_line );
                    
                    counter++;
                    
                 }
    
         }

    printf("Passwords cracked: %d\n", counter);

}